<?php
	$ini = parse_ini_file('../proxy.ini');
	if (!isset($ini['data.dir'])) die ('data.dir is not set in proxy.ini.  It must point to the local filesystem location where cache data should be stored.');
	if (!isset($ini['data.host'])) die ('data.host is not set in proxy.ini.   It must point to one or more hosts that data should be proxied from, comma separated.');

	define('DATA_DIR', $ini['data.dir'] . (endsWith($ini['data.dir'], '/') ? '' : '/'));
	define('DATA_HOSTS', $ini['data.host']);
	define('CACHE_EXPIRY_TIME', isset($ini['cache.expiry']) ? $ini['cache.expiry'] : 360);
	
	require_once('func_http_response_code.php');

	function startsWith($haystack, $needle)
	{
		$length = strlen($needle);
		return (substr($haystack, 0, $length) === $needle);
	}

	function endsWith($haystack, $needle)
	{
		$length = strlen($needle);

		return $length === 0 || 
			(substr($haystack, -$length) === $needle);
	}
	
	function getParamsToPass() {
		$parts = split("/", $_SERVER["REQUEST_URI"]);
		$toPass = $parts[count($parts)-1];
		return $toPass;
	}
	
	function &getParamsArray() {
		$params = getParamsToPass();
		$qnPsn = strpos($params, '?');
		$remainingUrl = substr($params, $qnPsn+1);
		parse_str($remainingUrl, $paramsArray);
		return $paramsArray;
	}
	
	function sanitiseToFilename($toPass) {
		$sanitised = str_ireplace(array('=', '?', '&', ','), array('=', '&', '&', ','), $toPass);
		if (!startsWith($sanitised, "_")) {
			$sanitised = "_" . $sanitised;
		}
		return $sanitised;
	}
	
	function str_replace_first($from, $to, $content)
	{
		$from = '/'.preg_quote($from, '/').'/';
		return preg_replace($from, $to, $content, 1);
	}
	
	function sanitiseToRemoteUrl($toPass) {
		$sanitised = $toPass;
		if (startsWith($toPass, "_")) {
			$length = strlen($sanitised)-1;
			$sanitised = substr($sanitised, 1, $length);
		}
		$sanitised = str_ireplace(array('=', '?', '&', ','), array('=', '&', '&', ','), $sanitised);
		$ampPos = strpos($sanitised, '&');
		$quePos = strpos($sanitised, '?');
		if ($quePos >= 0 || ($ampPos > 0 && $ampPos < $quePos)) {
			$sanitised = str_replace_first('&', '?', $sanitised);
		}
		return $sanitised;
	}

	function getCacheFilename($toPass) {
		$sanitised = sanitiseToFilename($toPass);
		$filename = DATA_DIR . $sanitised;
		return $filename;
	}
	
	function getCacheHeadersFilename($toPass) {
		$filename = getCacheFilename($toPass);
		$filename .= '_headers';
		return $filename;
	}

	function isCached($toPass) {
		$filename = getCacheFilename($toPass);
		
		if (file_exists($filename)) {
			return true;
		}
		return false;
	}
	
	function &getCacheData($toPass) {
		$filename = getCacheFilename($toPass);
		$contents = file_get_contents($filename);
		return $contents;
	}
	
	function getCacheFileTime($toPass) {
		$filename = getCacheFilename($toPass);
		$mtime = filemtime($filename);
		return $mtime;
	}
	
	function expiryMsecs() {
		return CACHE_EXPIRY_TIME * 60;
	}
	
	function isCachedFileExpired($toPass) {
		if (!isCached($toPass)) {
			return true;
		}
		$mtime = getCacheFileTime($toPass);
		$ctime = time();
		$age = $ctime - $mtime;
		
		$maxAge = expiryMsecs();
		
		if ($age > $maxAge) {
			return true;
		}
		return false;
	}

	function &getCacheHeaders($toPass) {
		$filename = getCacheHeadersFilename($toPass);
		$contents = file_get_contents($filename);
		$deserialized = unserialize($contents);
		return $deserialized;
	}
	
	function &cacheAs($url, $filename, $headerFilename, $output = false) {
		$hosts = split(",", DATA_HOSTS);
		$contents = NULL;
		for ($num = 0;$num < count($hosts);$num++) {
			$fullUrl = trim($hosts[$num]) . $url;
			if ($output) {
				echo ('Read from ' . $fullUrl . '<br />');
			}
		
			$context = stream_context_create();
			$contents = file_get_contents($fullUrl, FALSE);
			if ($contents == null || $contents == '') {
				//echo ('No response from ' . $fullUrl);
				continue;
			} else {
				$serializedHeaders = serialize($http_response_header);
				file_put_contents($filename, $contents);
				file_put_contents($headerFilename, $serializedHeaders);
			}
			return $contents;
		}
		return $contents;
	}
	
	function &cacheUrl($toPass) {
		$filename = getCacheFilename($toPass);
		$url = sanitiseToRemoteUrl($toPass);
		$headerFilename = getCacheHeadersFilename($toPass);
		
		$contents = cacheAs($url, $filename, $headerFilename);
		return $contents;
	}
	
	function outputFile($toPass) {
		$data = getCacheData($toPass);
		outputData($data);
	}
	
	function outputData($data) {
		echo($data);
	}
	
	function sendHeadersFor($toPass) {
		$headers = getCacheHeaders($toPass);
		if ($headers != null) {
			printHeaders($headers);
		} else {
			error_log('Headers was null for request ' . $toPass);
		}
	}
	
	function printHeaders(&$headers) {
		if ($headers == null) {
			error_log('Headers was null when trying to print.');
		}
		foreach($headers as $header) {
			if (startsWith($header, "Content-Type") || startsWith($header, "Content-Disposition")) {
				header($header);
			} else {
			}
		}
	}
	
	function recacheExistingFile($entry, $forceUpdate = false) {
		$existingCacheFilename = DATA_DIR . $entry;
		
		$mtime = filemtime($existingCacheFilename);
		$ctime = time();
		$age = $ctime - $mtime;

		$maxAge = expiryMsecs();
		
		$remoteUrl = sanitiseToRemoteUrl($entry);

		if ($age > $maxAge || $forceUpdate) {
			$targetCacheFilename = getCacheFilename($entry);
			$targetCacheHeaderFilename = getCacheHeadersFilename($entry);
			cacheAs($remoteUrl, $targetCacheFilename, $targetCacheHeaderFilename, true);
			echo ('Updated ' . $existingCacheFilename . ' from ' . $remoteUrl . '<br />');
		}
	}
	
	function getAllFiles() {
		if ($handle = opendir(DATA_DIR)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != "." && $entry != "..") {
					if (endsWith($entry, "_headers")) {
						continue;
					}
					recacheExistingFile($entry, true);
				}
			}
		}
		closedir($handle);
	}
	
	$toPass = getParamsToPass();
	//var_dump($_REQUEST);

	if (isset($_GET['REHASH'])) {
		getAllFiles();
	} else if (!isCachedFileExpired($toPass)) {
		sendHeadersFor($toPass);
		outputFile($toPass);
	} else {
		$retrieved = cacheUrl($toPass);
		if ($retrieved != null) {
			sendHeadersFor($toPass);
			outputData($retrieved);
		} else if (isCached($toPass)) {
			sendHeadersFor($toPass);
			outputFile($toPass);
		} else {
			// Send a 503 error (Service unavailable)
			http_response_code(503);
		}
	}
?>